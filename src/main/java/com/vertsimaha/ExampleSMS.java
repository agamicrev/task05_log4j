package com.vertsimaha;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class ExampleSMS {

  // Find your Account Sid and Token at twilio.com/user/account
  public static final String ACCOUNT_SID = "ACa5595ae0da9cd6cadde3fbdeb58f998e";
  public static final String AUTH_TOKEN = "0d96e34f891e4f0ff78c902d62aac8f8";

  public static void send(String str) {
    Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
    Message message = Message
        .creator(new PhoneNumber("+380992805142"), /*my phone number*/
            new PhoneNumber("+14028925345"), str).create(); /*attached to me number*/
  }
}
